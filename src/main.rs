use pest::Parser;
use pest_derive::Parser;

use std::{fs, vec};

#[derive(Parser)]
#[grammar = "../data/grammar.pest"]
struct FileParse;

fn main() {
    let data = fs::read_to_string("data/console.txt").unwrap();

    let pairs = FileParse::parse(Rule::file, &data).unwrap_or_else(|e| panic!("{}", e));

    let mut statements = vec![];

    for pair in pairs {
        pair.into_inner().for_each(|inner_pair| {
            match inner_pair.as_rule() {
                Rule::define_statement => {
                    let statement = handle_define(inner_pair);
                    statements.push(Statement::DefineStatement(statement));
                }
                Rule::comment => {
                    statements.push(Statement::Comment(inner_pair.as_str().to_string()));
                }
                Rule::register_statement => {}
                Rule::EOI => {}
                _ => {
                    unimplemented!("Unimplemented on root: `{:?}`", inner_pair);
                }
            };
        })
    }

    println!("{:#?}", statements);
}

#[allow(dead_code)]
#[derive(Debug)]
enum Statement {
    DefineStatement(DefineStatement),
    Comment(String),
}

#[allow(dead_code)]
#[derive(Debug)]
enum DefineStatement {
    DefineLabelType(LabelType),
    DefineClaimType(ClaimType),
}

#[allow(dead_code)]
#[derive(Debug)]
enum ExpresssionTemplatePart {
    String(String),
    Identifier(String),
}

#[derive(Debug)]
struct ExpresssionTemplate {
    expressions: Vec<ExpresssionTemplatePart>,
}

#[derive(Debug)]
struct ClaimType {
    name: String,
    expression_template: Option<ExpresssionTemplate>,
    nested_expression_template: Option<ExpresssionTemplate>,
    plural_name: Option<String>,
    totality: Option<String>,
    unicity: Option<String>,
    identity: Option<Vec<String>>,
}

#[derive(Debug)]
struct LabelType {
    name: String,
    scalar_type: String,
}

fn handle_define(pair: pest::iterators::Pair<Rule>) -> DefineStatement {
    let mut statement = None;

    pair.into_inner().for_each(|pair| match pair.as_rule() {
        Rule::define_label_type => {
            let res = handle_define_labeltype(pair);
            statement = Some(DefineStatement::DefineLabelType(res));
        }
        Rule::define_claim_type => {
            let res = handle_define_claimtype(pair);
            statement = Some(DefineStatement::DefineClaimType(res));
        }
        _ => unreachable!(),
    });

    statement.unwrap()
}

fn handle_define_labeltype(pair: pest::iterators::Pair<Rule>) -> LabelType {
    let mut labeltype = LabelType {
        name: String::new(),
        scalar_type: String::new(),
    };
    pair.into_inner().for_each(|pair| match pair.as_rule() {
        Rule::string => labeltype.name = pair.as_str().to_string(),
        Rule::scalar_type => labeltype.scalar_type = pair.as_str().to_string(),
        _ => unreachable!(),
    });
    labeltype
}

fn handle_define_claimtype(pair: pest::iterators::Pair<Rule>) -> ClaimType {
    let mut claimtype = ClaimType {
        name: String::new(),
        expression_template: None,
        nested_expression_template: None,
        plural_name: None,
        totality: None,
        unicity: None,
        identity: None,
    };

    pair.into_inner().for_each(|pair| match pair.as_rule() {
        Rule::path => claimtype.name = pair.as_str().to_string(),
        Rule::claim_type_body => {
            pair.into_inner().for_each(|pair| match pair.as_rule() {
                Rule::expression_template_line => {
                    let e = handle_expression_template(pair);
                    claimtype.expression_template = Some(e);
                }
                Rule::nested_expression_template_line => {
                    let e = handle_expression_template(pair);
                    claimtype.nested_expression_template = Some(e);
                }
                Rule::pluralName_line => {
                    let string = handle_string(pair);
                    claimtype.plural_name = Some(string);
                }
                Rule::totality_line => {
                    let string = handle_string(pair);
                    claimtype.totality = Some(string);
                }
                Rule::unicity_line => {
                    let string = handle_string(pair);
                    claimtype.unicity = Some(string);
                }
                Rule::identity_line => {
                    let strings = handle_string_array(pair);
                    claimtype.identity = Some(strings);
                }
                _ => {
                    unimplemented!("Unimplemented on claimtype_body: {}", pair.as_str());
                }
            });
        }
        _ => {
            unimplemented!("Unimplemented on ddfine_claimtype: {}", pair.as_str());
        }
    });
    claimtype
}

fn handle_expression_template(pair: pest::iterators::Pair<Rule>) -> ExpresssionTemplate {
    let mut expression_template = ExpresssionTemplate {
        expressions: Vec::new(),
    };
    pair.into_inner().for_each(|pair| match pair.as_rule() {
        Rule::string => expression_template
            .expressions
            .push(ExpresssionTemplatePart::String(pair.as_str().to_string())),
        Rule::identifier => {
            expression_template
                .expressions
                .push(ExpresssionTemplatePart::Identifier(
                    pair.as_str().to_string(),
                ))
        }
        _ => unreachable!(),
    });

    expression_template
}

fn handle_string(pair: pest::iterators::Pair<Rule>) -> String {
    let mut string = None;

    pair.into_inner().for_each(|pair| match pair.as_rule() {
        Rule::string => {
            if string.is_some() {
                panic!("Multiple strings in a string rule");
            }
            string = Some(pair.as_str().to_string())
        }
        _ => unreachable!(),
    });

    string.unwrap()
}

fn handle_string_array(pair: pest::iterators::Pair<Rule>) -> Vec<String> {
    let mut strings = vec![];

    pair.into_inner().for_each(|pair| match pair.as_rule() {
        Rule::string_array => {
            pair.into_inner().for_each(|pair| match pair.as_rule() {
                Rule::string => strings.push(pair.as_str().to_string()),
                _ => unreachable!(),
            });
        }
        _ => unreachable!(),
    });

    if strings.is_empty() {
        panic!("No strings in a string array rule");
    }

    strings
}
